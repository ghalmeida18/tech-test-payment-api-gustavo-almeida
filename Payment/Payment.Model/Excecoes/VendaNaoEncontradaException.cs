﻿
namespace Payment.Model.Excecoes
{
    public class VendaNaoEncontradaException : Exception
    {
        public VendaNaoEncontradaException(string mensagem) : base(mensagem) { }
    }
}
