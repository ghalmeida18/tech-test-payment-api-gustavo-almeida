﻿
namespace Payment.Model.Excecoes
{
    public class StatusInicialException : Exception
    {
        public StatusInicialException(string mensagem) : base(mensagem) { }
    }
}
