﻿
namespace Payment.Model.Excecoes
{
    public class CriacaoVendaException : Exception
    {
        public CriacaoVendaException(string mensagem) : base(mensagem) { }
    }
}
