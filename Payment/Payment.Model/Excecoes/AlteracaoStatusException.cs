﻿namespace Payment.Model.Excecoes
{
    public class AlteracaoStatusException : Exception
    {
        public AlteracaoStatusException(string mensagem) : base(mensagem) { }
    }
}
