﻿using FluentValidation;

namespace Payment.Model.Validators
{
    public class ProdutoValidator : AbstractValidator<ProdutoModel>
    {
        public ProdutoValidator()
        {
            RuleFor(ProdutoModel => ProdutoModel.Id).NotEmpty().NotNull();
            RuleFor(ProdutoModel => ProdutoModel.Preco).GreaterThan(0);
            RuleFor(ProdutoModel => ProdutoModel.Nome).NotEmpty().NotNull();
        }
    }
}
