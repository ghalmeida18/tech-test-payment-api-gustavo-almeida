﻿using FluentValidation;

namespace Payment.Model.Validators
{
    public class VendedorValidator : AbstractValidator<VendedorModel>
    {
        public VendedorValidator()
        {
            RuleFor(VendedorModel => VendedorModel.Id).NotEmpty().NotNull();
            RuleFor(VendedorModel => VendedorModel.Email).NotEmpty().NotNull();
            RuleFor(VendedorModel => VendedorModel.CPF).NotEmpty().NotNull();
            RuleFor(VendedorModel => VendedorModel.Nome).NotEmpty().NotNull();
            RuleFor(VendedorModel => VendedorModel.Telefone).NotNull().NotEmpty();
        }
    }
}
