﻿using FluentValidation;

namespace Payment.Model.Validators
{
    public class VendaValidator : AbstractValidator<VendaModel>
    {
        public VendaValidator()
        {
            RuleFor(v => v.Id).NotEmpty().NotNull();
            RuleFor(v => v.Data).NotEmpty().NotNull();
            RuleFor(v => v.Vendedor).NotNull();
            RuleFor(v => v.Produtos).NotNull().NotEmpty();
            RuleForEach(v => v.Produtos).SetValidator(new ProdutoValidator());
            RuleFor(v => v.Vendedor).SetValidator(new VendedorValidator());
        }
    }
}
