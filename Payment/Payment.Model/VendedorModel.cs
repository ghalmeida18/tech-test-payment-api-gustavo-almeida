﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace Payment.Model
{
    public class VendedorModel
    {
        [Key]
        public Guid Id { get; set; }
        public string? CPF { get; set; }
        public string? Nome { get; set; }
        public string? Email { get; set; }
        public string? Telefone { get; set; }
        [JsonIgnore]
        public Guid? VendaId { get; set;  }
    }
}
