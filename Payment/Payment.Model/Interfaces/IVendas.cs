﻿
namespace Payment.Model.Interfaces
{
    public interface IVenda
    {
        public VendaModel BuscarVendaExecutor(Guid id);
        public VendaModel InserirVendaExecutor(VendaModel venda);
        public VendaModel AtualizarVendaExecutor(VendaModel venda);
    }
}
