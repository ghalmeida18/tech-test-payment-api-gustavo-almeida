﻿

namespace Payment.Model.Interfaces
{
    public interface IVendaRepository
    {
        public VendaModel? Obter(Guid Id);
        public VendaModel Inserir(VendaModel venda);
        public VendaModel Alterar(VendaModel venda);
    }
}
