﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace Payment.Model
{
    public class ProdutoModel
    {
        [Key]
        public Guid Id { get; set; }
        public string? Nome { get; set; }
        public decimal Preco { get; set; }
        [JsonIgnore]
        public Guid? VendaId { get; set; }
    }
}
