﻿using Payment.Model.Status;
using System.ComponentModel.DataAnnotations;

namespace Payment.Model
{
    public class VendaModel
    {
        [Key]
        public Guid Id { get; set; }
        public DateTime Data { get; set; }
        public StatusVenda Status { get; set; }
        public virtual VendedorModel? Vendedor { get; set; }
        public IEnumerable<ProdutoModel>? Produtos { get; set; }
    }
}
