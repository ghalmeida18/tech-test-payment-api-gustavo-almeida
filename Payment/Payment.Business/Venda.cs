﻿using Payment.Model;
using Payment.Model.Excecoes;
using Payment.Model.Interfaces;
using Payment.Model.Status;
using Payment.Model.Validators;

namespace Payment.Business
{
    public class Venda : IVenda
    {
        private readonly IVendaRepository _vendaRepository;

        public Venda(IVendaRepository vendaRepository)
        {
            _vendaRepository = vendaRepository;
        }

        public VendaModel BuscarVendaExecutor(Guid id)
        {
            var venda =  _vendaRepository.Obter(id);

            return venda ?? throw new VendaNaoEncontradaException($"Não foi possível obter a venda solicitada com ID {id}.");
        }

        public VendaModel AtualizarVendaExecutor(VendaModel venda)
        {
            var vendaRegistrada = _vendaRepository.Obter(venda.Id) ?? throw new VendaNaoEncontradaException($"Não foi possível obter a venda solicitada com ID {venda.Id}.");

            PreencheIds(venda);

            switch (vendaRegistrada.Status)
            {
                case StatusVenda.AguardandoPagamento:
                    if (venda.Status != StatusVenda.PagamentoAprovado && venda.Status != StatusVenda.Cancelada && venda.Status != StatusVenda.AguardandoPagamento)
                        throw new AlteracaoStatusException($"Não é possivel alterar o status Aguardando Pagamento para {venda.Status}");
                    break;

                case StatusVenda.PagamentoAprovado:
                    if (venda.Status != StatusVenda.EnviadoParaTransportadora && venda.Status != StatusVenda.Cancelada && venda.Status != StatusVenda.PagamentoAprovado)
                        throw new AlteracaoStatusException($"Não é possivel alterar o status Pagamento Aprovado para {venda.Status}");
                    break;

                case StatusVenda.EnviadoParaTransportadora:
                    if(venda.Status != StatusVenda.Entregue && venda.Status != StatusVenda.EnviadoParaTransportadora)
                        throw new AlteracaoStatusException($"Não é possivel alterar o status Enviado Para Transportadora para {venda.Status}");
                    break;

            }

            _vendaRepository.Alterar(venda);

            return venda;
        }

        public VendaModel InserirVendaExecutor(VendaModel venda)
        {
            if (venda.Status != StatusVenda.AguardandoPagamento)
                throw new StatusInicialException($"Não é possível criar um registro de venda com status diferente de {StatusVenda.AguardandoPagamento}.");

            VendaValidator validator = new();
            var validacao = validator.Validate(venda);

            if (!validacao.IsValid)
                throw new CriacaoVendaException($"Não foi possível criar registro de venda: {validacao.ToString().Replace("\r\n", " ; ")}");

            PreencheIds(venda);
            return _vendaRepository.Inserir(venda);
        }

        #region Métodos Auxiliares

        private static void PreencheIds(VendaModel venda)
        {
            Guid vendaId = venda.Id;

            venda.Vendedor.VendaId = vendaId;

            foreach (var prod in venda.Produtos)
                prod.VendaId = vendaId;
        }

        #endregion
    }
}
