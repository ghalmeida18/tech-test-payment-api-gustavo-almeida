﻿using Microsoft.EntityFrameworkCore;
using Payment.Model;

namespace Payment.Data.Context
{
    public class DataContext : DbContext
    {
        public DbSet<VendaModel> Vendas { get; set; }
        public DbSet<VendedorModel> Vendedores { get; set; }
        public DbSet<ProdutoModel> Produtos { get; set; }

        public DataContext(DbContextOptions<DataContext> options) : base(options) { }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseInMemoryDatabase(databaseName: "AuthorDb");
        }
    }
}
