﻿using Payment.Data.Context;
using Payment.Model;
using Payment.Model.Interfaces;

namespace Payment.Data
{
    public class VendaRepository : IVendaRepository
    {
        private readonly DataContext _context;

        public VendaRepository(DataContext context)
        {
            this._context = context;
        }

        public VendaModel Alterar(VendaModel venda)
        {
            var _vendedor = _context.Vendedores.SingleOrDefault(x => x.VendaId == venda.Id) ;

            if(_vendedor != null)
            {
                _context.Vendedores.Remove(_vendedor);
                _context.SaveChanges();
            }

            var _produtos = _context.Produtos.Where(x => x.VendaId == venda.Id);

            if (_produtos != null && _produtos.Any())
            {
                _context.Produtos.RemoveRange(_produtos);
                _context.SaveChanges();
            }

            var _venda = _context.Vendas.SingleOrDefault(x => x.Id == venda.Id);

            if (_venda != null)
            {
                _context.Vendas.Remove(_venda);
                _context.SaveChanges();
            }
            _context.Add(venda);
            _context.SaveChanges();

            return venda;
        }

        public VendaModel Inserir(VendaModel venda)
        {
            _context.Add(venda);
            _context.SaveChanges();

            return venda;
        }

        public VendaModel? Obter(Guid id)
        {
            var venda = _context
                .Vendas
                .Where(ev => ev.Id.Equals(id))
                .FirstOrDefault();

            if (venda != default)
            {
                venda.Vendedor = _context.Vendedores.Where(v => v.VendaId.Equals(id)).FirstOrDefault();
                venda.Produtos = _context.Produtos.Where(p => p.VendaId.Equals(id)).ToList();
            }

            return venda;
        }
    }
}
