using Microsoft.AspNetCore.Mvc;
using Payment.Model;
using Payment.Model.Excecoes;
using Payment.Model.Interfaces;

namespace Payment.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class VendaController : ControllerBase
    {
        private readonly IVenda _venda;

        public VendaController(IVenda venda)
        {
            this._venda = venda;
        }

        /// <summary>
        /// Busca uma venda por identificador
        /// </summary>
        [HttpGet("{id}", Name = "BuscaVenda")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult<VendaModel> Get(Guid id)
        {
            try
            {
                return _venda.BuscarVendaExecutor(id);
            }
            catch(VendaNaoEncontradaException e)
            {
                return NotFound(new { Erro = e.Message });
            }
        }

        /// <summary>
        /// Insere uma venda no sitema
        /// </summary>
        [HttpPost(Name = "InserirVenda")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<VendaModel> Post(VendaModel venda)
        {
            try
            {
                return _venda.InserirVendaExecutor(venda);
            }
            catch (StatusInicialException e)
            {
                return BadRequest(new { Erro = e.Message });
            }
            catch (CriacaoVendaException e)
            {
                return BadRequest(new { Erro = e.Message });
            }
        }

        /// <summary>
        /// Alterar Venda
        /// </summary>
        [HttpPatch(Name = "AlterarVenda")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<VendaModel> Patch(VendaModel venda)
        {
            try
            {
                return (new ActionResult<VendaModel>(_venda.AtualizarVendaExecutor(venda)));
            }
            catch (AlteracaoStatusException e)
            {
                return BadRequest(new { Erro = e.Message });
            }
        }
    }
}