using Microsoft.EntityFrameworkCore;
using Payment.Business;
using Payment.Data;
using Payment.Data.Context;
using Payment.Model.Interfaces;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddTransient<IVenda, Venda>();
builder.Services.AddTransient<IVendaRepository, VendaRepository>();
builder.Services.AddDbContext<DataContext>(opt => opt.UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking));

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
