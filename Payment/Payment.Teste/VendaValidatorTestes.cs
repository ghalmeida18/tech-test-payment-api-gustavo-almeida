using FluentValidation;
using Payment.Model;
using Payment.Model.Status;
using Payment.Model.Validators;
using Payment.Teste.Builders;
using Xunit.Sdk;

namespace Payment.Teste
{
    [TestClass]
    public class VendaValidatorTestes
    {
        [TestMethod]
        public void ValidarVenda_OK()
        {
            var _vendaBuider = new VendaBuilder();
            var _produtoBuilder = new ProdutoBuilder(); 
            var _vendedorBuilder = new VendedorBuilder();


            #region Builder
            _produtoBuilder.ComId(Guid.NewGuid());
            _produtoBuilder.ComVendaId(Guid.NewGuid());
            _produtoBuilder.ComPreco(10);
            _produtoBuilder.ComNome("Nome Teste");

            var produto = _produtoBuilder.Build();

            _vendedorBuilder.ComId(Guid.NewGuid());
            _vendedorBuilder.ComVendaId(Guid.NewGuid());
            _vendedorBuilder.ComTelefone("31999999999");
            _vendedorBuilder.ComNome("Nome Teste");
            _vendedorBuilder.ComCPF("754.966.340-83");
            _vendedorBuilder.ComEmail("email@teste.com");

            var vendedor = _vendedorBuilder.Build();

            _vendaBuider.ComId(Guid.NewGuid());
            _vendaBuider.ComData(DateTime.Now);
            _vendaBuider.ComStatus(StatusVenda.PagamentoAprovado);
            _vendaBuider.ComVendedor(vendedor);
            _vendaBuider.ComProdutos(new List<ProdutoModel>() { produto  });

            var venda = _vendaBuider.Build();


            #endregion

            #region Validator
            var validator = new VendaValidator();
            var result = validator.Validate(venda);
            #endregion

            #region Asserts
            Assert.IsTrue(result.IsValid);
            Assert.IsTrue(result.Errors.Count() == 0);
            #endregion
        }

        [TestMethod]
        public void ValidarVenda_IdVazio_NOK()
        {
            var _vendaBuider = new VendaBuilder();
            var _produtoBuilder = new ProdutoBuilder();
            var _vendedorBuilder = new VendedorBuilder();


            #region Builder
            _produtoBuilder.ComId(Guid.NewGuid());
            _produtoBuilder.ComVendaId(Guid.NewGuid());
            _produtoBuilder.ComPreco(10);
            _produtoBuilder.ComNome("Nome Teste");

            var produto = _produtoBuilder.Build();

            _vendedorBuilder.ComId(Guid.NewGuid());
            _vendedorBuilder.ComVendaId(Guid.NewGuid());
            _vendedorBuilder.ComTelefone("31999999999");
            _vendedorBuilder.ComNome("Nome Teste");
            _vendedorBuilder.ComCPF("754.966.340-83");
            _vendedorBuilder.ComEmail("email@teste.com");

            var vendedor = _vendedorBuilder.Build();

            _vendaBuider.ComId(Guid.Empty);
            _vendaBuider.ComData(DateTime.Now);
            _vendaBuider.ComStatus(StatusVenda.PagamentoAprovado);
            _vendaBuider.ComVendedor(vendedor);
            _vendaBuider.ComProdutos(new List<ProdutoModel>() { produto });

            var venda = _vendaBuider.Build();


            #endregion

            #region Validator
            var validator = new VendaValidator();
            var result = validator.Validate(venda);
            #endregion

            #region Asserts
            Assert.IsFalse(result.IsValid);
            Assert.IsTrue(result.Errors.Count != 0);
            #endregion
        }

        [TestMethod]
        public void ValidarVenda_DataMin_NOK()
        {
            var _vendaBuider = new VendaBuilder();
            var _produtoBuilder = new ProdutoBuilder();
            var _vendedorBuilder = new VendedorBuilder();


            #region Builder
            _produtoBuilder.ComId(Guid.NewGuid());
            _produtoBuilder.ComVendaId(Guid.NewGuid());
            _produtoBuilder.ComPreco(10);
            _produtoBuilder.ComNome("Nome Teste");

            var produto = _produtoBuilder.Build();

            _vendedorBuilder.ComId(Guid.NewGuid());
            _vendedorBuilder.ComVendaId(Guid.NewGuid());
            _vendedorBuilder.ComTelefone("31999999999");
            _vendedorBuilder.ComNome("Nome Teste");
            _vendedorBuilder.ComCPF("754.966.340-83");
            _vendedorBuilder.ComEmail("email@teste.com");

            var vendedor = _vendedorBuilder.Build();

            _vendaBuider.ComId(Guid.NewGuid());
            _vendaBuider.ComData(DateTime.MinValue);
            _vendaBuider.ComStatus(StatusVenda.PagamentoAprovado);
            _vendaBuider.ComVendedor(vendedor);
            _vendaBuider.ComProdutos(new List<ProdutoModel>() { produto });

            var venda = _vendaBuider.Build();


            #endregion

            #region Validator
            var validator = new VendaValidator();
            var result = validator.Validate(venda);
            #endregion

            #region Asserts
            Assert.IsFalse(result.IsValid);
            Assert.IsTrue(result.Errors.Count != 0);
            #endregion
        }

        [TestMethod]
        public void ValidarVenda_VendedorNulo_NOK()
        {
            var _vendaBuider = new VendaBuilder();
            var _produtoBuilder = new ProdutoBuilder();


            #region Builder
            _produtoBuilder.ComId(Guid.NewGuid());
            _produtoBuilder.ComVendaId(Guid.NewGuid());
            _produtoBuilder.ComPreco(10);
            _produtoBuilder.ComNome("Nome Teste");

            var produto = _produtoBuilder.Build();

            _vendaBuider.ComId(Guid.NewGuid());
            _vendaBuider.ComData(DateTime.Now);
            _vendaBuider.ComStatus(StatusVenda.PagamentoAprovado);
            _vendaBuider.ComVendedor(null);
            _vendaBuider.ComProdutos(new List<ProdutoModel>() { produto });

            var venda = _vendaBuider.Build();


            #endregion

            #region Validator
            var validator = new VendaValidator();
            var result = validator.Validate(venda);
            #endregion

            #region Asserts
            Assert.IsFalse(result.IsValid);
            Assert.IsTrue(result.Errors.Count != 0);
            #endregion
        }

        [TestMethod]
        public void ValidarVenda_ProdutoNulo_NOK()
        {
            var _vendaBuider = new VendaBuilder();
            var _vendedorBuilder = new VendedorBuilder();


            #region Builder
            _vendedorBuilder.ComId(Guid.NewGuid());
            _vendedorBuilder.ComVendaId(Guid.NewGuid());
            _vendedorBuilder.ComTelefone("31999999999");
            _vendedorBuilder.ComNome("Nome Teste");
            _vendedorBuilder.ComCPF("754.966.340-83");
            _vendedorBuilder.ComEmail("email@teste.com");

            var vendedor = _vendedorBuilder.Build();

            _vendaBuider.ComId(Guid.NewGuid());
            _vendaBuider.ComData(DateTime.Now);
            _vendaBuider.ComStatus(StatusVenda.PagamentoAprovado);
            _vendaBuider.ComVendedor(vendedor);
            _vendaBuider.ComProdutos(null);

            var venda = _vendaBuider.Build();


            #endregion

            #region Validator
            var validator = new VendaValidator();
            var result = validator.Validate(venda);
            #endregion

            #region Asserts
            Assert.IsFalse(result.IsValid);
            Assert.IsTrue(result.Errors.Count != 0);
            #endregion
        }

        [TestMethod]
        public void ValidarVenda_ProdutosVazio_NOK()
        {
            var _vendaBuider = new VendaBuilder();
            var _vendedorBuilder = new VendedorBuilder();


            #region Builder
          
            _vendedorBuilder.ComId(Guid.NewGuid());
            _vendedorBuilder.ComVendaId(Guid.NewGuid());
            _vendedorBuilder.ComTelefone("31999999999");
            _vendedorBuilder.ComNome("Nome Teste");
            _vendedorBuilder.ComCPF("754.966.340-83");
            _vendedorBuilder.ComEmail("email@teste.com");

            var vendedor = _vendedorBuilder.Build();

            _vendaBuider.ComId(Guid.NewGuid());
            _vendaBuider.ComData(DateTime.MinValue);
            _vendaBuider.ComStatus(StatusVenda.PagamentoAprovado);
            _vendaBuider.ComVendedor(vendedor);
            _vendaBuider.ComProdutos(new List<ProdutoModel>() { });

            var venda = _vendaBuider.Build();


            #endregion

            #region Validator
            var validator = new VendaValidator();
            var result = validator.Validate(venda);
            #endregion

            #region Asserts
            Assert.IsFalse(result.IsValid);
            Assert.IsTrue(result.Errors.Count != 0);
            #endregion
        }

    }
}