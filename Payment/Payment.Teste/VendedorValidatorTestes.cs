using Payment.Model.Validators;
using Payment.Teste.Builders;
using Xunit.Sdk;

namespace Payment.Teste
{
    [TestClass]
    public class VendedorValidatorTestes
    {
        [TestMethod]
        public void ValidarVendedor_IdVazio_NOK()
        {
            var _vendedorBuilder = new VendedorBuilder();

            #region Builder
            _vendedorBuilder.ComId(Guid.Empty);
            _vendedorBuilder.ComVendaId(Guid.NewGuid());
            _vendedorBuilder.ComTelefone("31999999999");
            _vendedorBuilder.ComNome("Nome Teste");
            _vendedorBuilder.ComCPF("754.966.340-83");
            _vendedorBuilder.ComEmail("email@teste.com");

            var vendedor = _vendedorBuilder.Build();
            #endregion

            #region Validator
            var validator = new VendedorValidator();
            var result = validator.Validate(vendedor);
            #endregion

            #region Asserts
            Assert.IsFalse(result.IsValid);
            Assert.IsTrue(result.Errors.Count != 0);
            #endregion
        }

        [TestMethod]
        public void ValidarVendedor_TelefoneVazio_NOK()
        {
            var _vededorBuilder = new VendedorBuilder();

            #region Builder
            _vededorBuilder.ComId(Guid.NewGuid());
            _vededorBuilder.ComVendaId(Guid.NewGuid());
            _vededorBuilder.ComTelefone("");
            _vededorBuilder.ComNome("Nome Teste");
            _vededorBuilder.ComCPF("754.966.340-83");
            _vededorBuilder.ComEmail("email@teste.com");

            var vendedor = _vededorBuilder.Build();
            #endregion

            #region Validator
            var validator = new VendedorValidator();
            var result = validator.Validate(vendedor);
            #endregion

            #region Asserts
            Assert.IsFalse(result.IsValid);
            Assert.IsTrue(result.Errors.Count != 0);
            #endregion
        }

        [TestMethod]
        public void ValidarVendedor_TelefoneNulo_NOK()
        {
            var _vededorBuilder = new VendedorBuilder();

            #region Builder
            _vededorBuilder.ComId(Guid.NewGuid());
            _vededorBuilder.ComVendaId(Guid.NewGuid());
            _vededorBuilder.ComTelefone(null);
            _vededorBuilder.ComNome("Nome Teste");
            _vededorBuilder.ComCPF("754.966.340-83");
            _vededorBuilder.ComEmail("email@teste.com");

            var vendedor = _vededorBuilder.Build();
            #endregion

            #region Validator
            var validator = new VendedorValidator();
            var result = validator.Validate(vendedor);
            #endregion

            #region Asserts
            Assert.IsFalse(result.IsValid);
            Assert.IsTrue(result.Errors.Count != 0);
            #endregion
        }

        [TestMethod]
        public void ValidarVendedor_NomeNulo_NOK()
        {
            var _vededorBuilder = new VendedorBuilder();

            #region Builder
            _vededorBuilder.ComId(Guid.NewGuid());
            _vededorBuilder.ComVendaId(Guid.NewGuid());
            _vededorBuilder.ComTelefone("31999999999");
            _vededorBuilder.ComNome(null);
            _vededorBuilder.ComCPF("754.966.340-83");
            _vededorBuilder.ComEmail("email@teste.com");

            var vendedor = _vededorBuilder.Build();
            #endregion

            #region Validator
            var validator = new VendedorValidator();
            var result = validator.Validate(vendedor);
            #endregion

            #region Asserts
            Assert.IsFalse(result.IsValid);
            Assert.IsTrue(result.Errors.Count != 0);
            #endregion
        }

        [TestMethod]
        public void ValidarVendedor_NomeVazio_NOK()
        {
            var _vededorBuilder = new VendedorBuilder();

            #region Builder
            _vededorBuilder.ComId(Guid.NewGuid());
            _vededorBuilder.ComVendaId(Guid.NewGuid());
            _vededorBuilder.ComTelefone("31999999999");
            _vededorBuilder.ComNome("");
            _vededorBuilder.ComCPF("754.966.340-83");
            _vededorBuilder.ComEmail("email@teste.com");

            var vendedor = _vededorBuilder.Build();
            #endregion

            #region Validator
            var validator = new VendedorValidator();
            var result = validator.Validate(vendedor);
            #endregion

            #region Asserts
            Assert.IsFalse(result.IsValid);
            Assert.IsTrue(result.Errors.Count != 0);
            #endregion
        }

        [TestMethod]
        public void ValidarVendedor_CPFVazio_NOK()
        {
            var _vededorBuilder = new VendedorBuilder();

            #region Builder
            _vededorBuilder.ComId(Guid.NewGuid());
            _vededorBuilder.ComVendaId(Guid.NewGuid());
            _vededorBuilder.ComTelefone("31999999999");
            _vededorBuilder.ComNome("Nome Teste");
            _vededorBuilder.ComCPF("");
            _vededorBuilder.ComEmail("email@teste.com");

            var vendedor = _vededorBuilder.Build();
            #endregion

            #region Validator
            var validator = new VendedorValidator();
            var result = validator.Validate(vendedor);
            #endregion

            #region Asserts
            Assert.IsFalse(result.IsValid);
            Assert.IsTrue(result.Errors.Count != 0);
            #endregion
        }

        [TestMethod]
        public void ValidarVendedor_CPFNulo_NOK()
        {
            var _vededorBuilder = new VendedorBuilder();

            #region Builder
            _vededorBuilder.ComId(Guid.NewGuid());
            _vededorBuilder.ComVendaId(Guid.NewGuid());
            _vededorBuilder.ComTelefone("31999999999");
            _vededorBuilder.ComNome("Nome Teste");
            _vededorBuilder.ComCPF(null);
            _vededorBuilder.ComEmail("email@teste.com");

            var vendedor = _vededorBuilder.Build();
            #endregion

            #region Validator
            var validator = new VendedorValidator();
            var result = validator.Validate(vendedor);
            #endregion

            #region Asserts
            Assert.IsFalse(result.IsValid);
            Assert.IsTrue(result.Errors.Count != 0);
            #endregion
        }

        [TestMethod]
        public void ValidarVendedor_EmailVazio_NOK()
        {
            var _vededorBuilder = new VendedorBuilder();

            #region Builder
            _vededorBuilder.ComId(Guid.NewGuid());
            _vededorBuilder.ComVendaId(Guid.NewGuid());
            _vededorBuilder.ComTelefone("31999999999");
            _vededorBuilder.ComNome("Nome Teste");
            _vededorBuilder.ComCPF("754.966.340-83");
            _vededorBuilder.ComEmail("");

            var vendedor = _vededorBuilder.Build();
            #endregion

            #region Validator
            var validator = new VendedorValidator();
            var result = validator.Validate(vendedor);
            #endregion

            #region Asserts
            Assert.IsFalse(result.IsValid);
            Assert.IsTrue(result.Errors.Count != 0);
            #endregion
        }

        [TestMethod]
        public void ValidarVendedor_EmailNulo_NOK()
        {
            var _vededorBuilder = new VendedorBuilder();

            #region Builder
            _vededorBuilder.ComId(Guid.NewGuid());
            _vededorBuilder.ComVendaId(Guid.NewGuid());
            _vededorBuilder.ComTelefone("31999999999");
            _vededorBuilder.ComNome("Nome Teste");
            _vededorBuilder.ComCPF("754.966.340-83");
            _vededorBuilder.ComEmail(null);

            var vendedor = _vededorBuilder.Build();
            #endregion

            #region Validator
            var validator = new VendedorValidator();
            var result = validator.Validate(vendedor);
            #endregion

            #region Asserts
            Assert.IsFalse(result.IsValid);
            Assert.IsTrue(result.Errors.Count != 0);
            #endregion
        }


    }
}