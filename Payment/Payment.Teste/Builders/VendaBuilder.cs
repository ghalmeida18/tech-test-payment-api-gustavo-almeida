﻿using Payment.Model;
using Payment.Model.Status;
using Payment.Teste.Interfaces;

namespace Payment.Teste.Builders
{
    public class VendaBuilder : IBuilder<VendaModel>
    {
        private readonly VendaModel venda = new();

        public VendaModel Build()
        {
            return venda;
        }

        public void ComId(Guid id)
        {
            venda.Id = id;
        }

        public void ComData(DateTime data)
        {
            venda.Data = data;
        }

        public void ComStatus(StatusVenda status)
        {
            venda.Status = status;
        }

        public void ComVendedor(VendedorModel vendedor)
        {
            venda.Vendedor = vendedor;
        }

        public void ComProdutos(IEnumerable<ProdutoModel> produtos)
        {
            venda.Produtos = produtos;
        }
    }
}
