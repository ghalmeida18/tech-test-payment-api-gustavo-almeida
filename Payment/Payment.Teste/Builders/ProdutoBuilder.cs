﻿using Payment.Model;
using Payment.Teste.Interfaces;

namespace Payment.Teste.Builders
{
    public class ProdutoBuilder : IBuilder<ProdutoModel>
    {
        private readonly ProdutoModel produto = new();

        public ProdutoModel Build()
        {
            return produto;
        }

        public void ComId(Guid id)
        {
            produto.Id = id;
        }

        public void ComVendaId(Guid id)
        {
            produto.VendaId = id;
        }

        public void ComNome(string nome)
        {
            produto.Nome = nome;
        }

        public void ComPreco(decimal preco)
        {
            produto.Preco = preco;
        }
    }
}
