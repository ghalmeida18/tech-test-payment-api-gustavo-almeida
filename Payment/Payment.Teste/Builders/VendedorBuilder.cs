﻿using Payment.Model;
using Payment.Teste.Interfaces;

namespace Payment.Teste.Builders
{
    public class VendedorBuilder : IBuilder<VendedorModel>
    {
        private VendedorModel vendedor = new VendedorModel();

        public VendedorModel Build()
        {
            return vendedor;
        }

        public void ComId(Guid id)
        {
            vendedor.Id = id;
        }

        public void ComCPF(string CPF)
        {
            vendedor.CPF = CPF;
        }
        public void ComNome(string nome)
        {
            vendedor.Nome = nome;
        }

        public void ComEmail(string email)
        {
            vendedor.Email = email;
        }

        public void ComVendaId(Guid id)
        {
            vendedor.VendaId = id;
        }

        public void ComTelefone(string telefone)
        {
            vendedor.Telefone = telefone;
        }
    }
}
