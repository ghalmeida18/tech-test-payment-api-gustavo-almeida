using FluentValidation;
using Payment.Model.Validators;
using Payment.Teste.Builders;
using Xunit.Sdk;

namespace Payment.Teste
{
    [TestClass]
    public class ProdutoValidatorTestes
    {
        [TestMethod]
        public void ValidarProduto_IdVazio_NOK()
        {
            var _produtoBuilder = new ProdutoBuilder();

            #region Builder
            _produtoBuilder.ComId(Guid.Empty);
            _produtoBuilder.ComVendaId(Guid.NewGuid());
            _produtoBuilder.ComPreco(10);
            _produtoBuilder.ComNome("Nome Teste");

            var produto = _produtoBuilder.Build();
            #endregion

            #region Validator
            var validator = new ProdutoValidator();
            var result = validator.Validate(produto);
            #endregion

            #region Asserts
            Assert.IsFalse(result.IsValid);
            Assert.IsTrue(result.Errors.Count != 0);
            #endregion
        }

        [TestMethod]
        public void ValidarProduto_PrecoZero_NOK()
        {
            var _produtoBuilder = new ProdutoBuilder();

            #region Builder
            _produtoBuilder.ComId(Guid.NewGuid());
            _produtoBuilder.ComVendaId(Guid.NewGuid());
            _produtoBuilder.ComPreco(0);
            _produtoBuilder.ComNome("Nome Teste");

            var produto = _produtoBuilder.Build();
            #endregion

            #region Validator
            var validator = new ProdutoValidator();
            var result = validator.Validate(produto);
            #endregion

            #region Asserts
            Assert.IsFalse(result.IsValid);
            Assert.IsTrue(result.Errors.Count != 0);
            #endregion
        }

        [TestMethod]
        public void ValidarProduto_PrecoNegativo_NOK()
        {
            var _produtoBuilder = new ProdutoBuilder();

            #region Builder
            _produtoBuilder.ComId(Guid.NewGuid());
            _produtoBuilder.ComVendaId(Guid.NewGuid());
            _produtoBuilder.ComPreco(-20);
            _produtoBuilder.ComNome("Nome Teste");

            var produto = _produtoBuilder.Build();
            #endregion

            #region Validator
            var validator = new ProdutoValidator();
            var result = validator.Validate(produto);
            #endregion

            #region Asserts
            Assert.IsFalse(result.IsValid);
            Assert.IsTrue(result.Errors.Count != 0);
            #endregion
        }

        [TestMethod]
        public void ValidarProduto_NomeNull_NOK()
        {
            var _produtoBuilder = new ProdutoBuilder();

            #region Builder
            _produtoBuilder.ComId(Guid.NewGuid());
            _produtoBuilder.ComVendaId(Guid.NewGuid());
            _produtoBuilder.ComPreco(10);
            _produtoBuilder.ComNome(null);

            var produto = _produtoBuilder.Build();
            #endregion

            #region Validator
            var validator = new ProdutoValidator();
            var result = validator.Validate(produto);
            #endregion

            #region Asserts
            Assert.IsFalse(result.IsValid);
            Assert.IsTrue(result.Errors.Count != 0);
            #endregion
        }

        [TestMethod]
        public void ValidarProduto_NomeVazio_NOK()
        {
            var _produtoBuilder = new ProdutoBuilder();

            #region Builder
            _produtoBuilder.ComId(Guid.NewGuid());
            _produtoBuilder.ComVendaId(Guid.NewGuid());
            _produtoBuilder.ComPreco(10);
            _produtoBuilder.ComNome("");

            var produto = _produtoBuilder.Build();
            #endregion

            #region Validator
            var validator = new ProdutoValidator();
            var result = validator.Validate(produto);
            #endregion

            #region Asserts
            Assert.IsFalse(result.IsValid);
            Assert.IsTrue(result.Errors.Count != 0);
            #endregion
        }

        [TestMethod]
        public void ValidarProduto_OK()
        {
            var _produtoBuilder = new ProdutoBuilder();

            #region Builder
            _produtoBuilder.ComId(Guid.NewGuid());
            _produtoBuilder.ComVendaId(Guid.NewGuid());
            _produtoBuilder.ComPreco(10);
            _produtoBuilder.ComNome("Nome Teste");

            var produto = _produtoBuilder.Build();
            #endregion

            #region Validator
            var validator = new ProdutoValidator();
            var result = validator.Validate(produto);
            #endregion

            #region Asserts
            Assert.IsTrue(result.IsValid);
            Assert.IsTrue(result.Errors.Count == 0);
            #endregion
        }
    }
}