﻿
namespace Payment.Teste.Interfaces
{
    public interface IBuilder<T> where T : class
    {
        void ComId(Guid id);
        T Build();
    }
}
