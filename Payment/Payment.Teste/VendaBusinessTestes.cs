﻿using Moq;
using Payment.Business;
using Payment.Model;
using Payment.Model.Excecoes;
using Payment.Model.Interfaces;
using Payment.Model.Status;
using Payment.Teste.Builders;

namespace Payment.Teste
{
    [TestClass]
    public class VendaBusinessTestes
    {
        private readonly Mock<IVendaRepository> _vendaRepository = new();

        [TestMethod]
        public void VendaBusiness_OK()
        {
            var vendaPadrao = ObterVendaPadrao();

            _vendaRepository.Setup(s => s.Obter(It.IsAny<Guid>())).Returns(vendaPadrao);
            var vendaBusiness = new Venda(_vendaRepository.Object);

            var retorno = vendaBusiness.BuscarVendaExecutor(Guid.NewGuid());

            Assert.IsTrue(retorno != null);
            Assert.IsTrue(retorno.Id != Guid.Empty);
            Assert.IsTrue(retorno.Vendedor != null);
            Assert.IsTrue(retorno.Vendedor.Id != Guid.Empty);
            Assert.IsTrue(retorno.Produtos != null);
            Assert.IsTrue(retorno.Produtos.Any());
        }

        [TestMethod]
        public void VendaBusiness_RetornoNulo_NOK()
        {
            var vendaPadrao = ObterVendaPadrao();

            _vendaRepository.Setup(s => s.Obter(It.IsAny<Guid>()));
            var vendaBusiness = new Venda(_vendaRepository.Object);

            Assert.ThrowsException<VendaNaoEncontradaException>(() => vendaBusiness.BuscarVendaExecutor(Guid.NewGuid()));
        }

        [TestMethod]
        public void VendaBusiness_InserirVenda_OK()
        {
            var vendaPadrao = ObterVendaPadrao();

            _vendaRepository.Setup(s => s.Inserir(It.IsAny<VendaModel>())).Returns(vendaPadrao);
            var vendaBusiness = new Venda(_vendaRepository.Object);
            var retornoInsert = vendaBusiness.InserirVendaExecutor(vendaPadrao);

            Assert.AreEqual(retornoInsert.Id, vendaPadrao.Id);
        }

        [TestMethod]
        public void VendaBusiness_InserirVenda_IdInvalido_NOK()
        {
            var vendaPadrao = ObterVendaPadrao();

            var vendaBusiness = new Venda(_vendaRepository.Object);

            vendaPadrao.Id = Guid.Empty;

            Assert.ThrowsException<CriacaoVendaException>(() => vendaBusiness.InserirVendaExecutor(vendaPadrao));

        }

        [TestMethod]
        public void VendaBusiness_InserirVenda_SemProdutos_NOK()
        {
            var vendaPadrao = ObterVendaPadrao();

            var vendaBusiness = new Venda(_vendaRepository.Object);

            vendaPadrao.Produtos = new List<ProdutoModel>();

            Assert.ThrowsException<CriacaoVendaException>(() => vendaBusiness.InserirVendaExecutor(vendaPadrao));

        }

        [TestMethod]
        public void VendaBusiness_InserirVenda_VededorNomeNulo_NOK()
        {
            var vendaPadrao = ObterVendaPadrao();

            var vendaBusiness = new Venda(_vendaRepository.Object);

            vendaPadrao.Vendedor.Nome = null;

            Assert.ThrowsException<CriacaoVendaException>(() => vendaBusiness.InserirVendaExecutor(vendaPadrao));

        }

        [TestMethod]
        public void VendaBusiness_InserirVenda_ProdutoNomeNulo_NOK()
        {
            var vendaPadrao = ObterVendaPadrao();

            var vendaBusiness = new Venda(_vendaRepository.Object);

            vendaPadrao.Produtos.First().Nome = null;

            Assert.ThrowsException<CriacaoVendaException>(() => vendaBusiness.InserirVendaExecutor(vendaPadrao));

        }

        [TestMethod]
        public void VendaBusiness_AtualizarVenda_OK()
        {
            var vendaPadrao = ObterVendaPadrao();

            _vendaRepository.Setup(s => s.Obter(It.IsAny<Guid>())).Returns(vendaPadrao);

            var vendaBusiness = new Venda(_vendaRepository.Object);

            var retorno = vendaBusiness.AtualizarVendaExecutor(vendaPadrao);

            Assert.AreEqual(vendaPadrao.Id, retorno.Id);

        }

        [TestMethod]
        public void VendaBusiness_AtualizarVenda_AguardandoPagamento_EnviadoParaTransportadora_NOK()
        {
            var vendaPadrao = ObterVendaPadrao();
            var vendaAtualizada = ObterVendaPadrao();

            vendaAtualizada.Status = StatusVenda.EnviadoParaTransportadora;

            _vendaRepository.Setup(s => s.Obter(It.IsAny<Guid>())).Returns(vendaPadrao);

            var vendaBusiness = new Venda(_vendaRepository.Object);

            Assert.ThrowsException<AlteracaoStatusException>(() => vendaBusiness.AtualizarVendaExecutor(vendaAtualizada));
        }

        [TestMethod]
        public void VendaBusiness_AtualizarVenda_AguardandoPagamento_Entregue_NOK()
        {
            var vendaPadrao = ObterVendaPadrao();
            var vendaAtualizada = ObterVendaPadrao();

            vendaAtualizada.Status = StatusVenda.Entregue;

            _vendaRepository.Setup(s => s.Obter(It.IsAny<Guid>())).Returns(vendaPadrao);

            var vendaBusiness = new Venda(_vendaRepository.Object);

            Assert.ThrowsException<AlteracaoStatusException>(() => vendaBusiness.AtualizarVendaExecutor(vendaAtualizada));
        }

        [TestMethod]
        public void VendaBusiness_AtualizarVenda_PagamentoAprovado_AguardandoPagamento_NOK()
        {
            var vendaPadrao = ObterVendaPadrao();
            var vendaAtualizada = ObterVendaPadrao();

            vendaPadrao.Status = StatusVenda.PagamentoAprovado;
            vendaAtualizada.Status = StatusVenda.AguardandoPagamento;

            _vendaRepository.Setup(s => s.Obter(It.IsAny<Guid>())).Returns(vendaPadrao);

            var vendaBusiness = new Venda(_vendaRepository.Object);

            Assert.ThrowsException<AlteracaoStatusException>(() => vendaBusiness.AtualizarVendaExecutor(vendaAtualizada));
        }

        [TestMethod]
        public void VendaBusiness_AtualizarVenda_PagamentoAprovado_Entregue_NOK()
        {
            var vendaPadrao = ObterVendaPadrao();
            var vendaAtualizada = ObterVendaPadrao();

            vendaPadrao.Status = StatusVenda.PagamentoAprovado;
            vendaAtualizada.Status = StatusVenda.Entregue;

            _vendaRepository.Setup(s => s.Obter(It.IsAny<Guid>())).Returns(vendaPadrao);

            var vendaBusiness = new Venda(_vendaRepository.Object);

            Assert.ThrowsException<AlteracaoStatusException>(() => vendaBusiness.AtualizarVendaExecutor(vendaAtualizada));
        }

        [TestMethod]
        public void VendaBusiness_AtualizarVenda_Enviado_AguardandoPagamento_NOK()
        {
            var vendaPadrao = ObterVendaPadrao();
            var vendaAtualizada = ObterVendaPadrao();

            vendaPadrao.Status = StatusVenda.EnviadoParaTransportadora;
            vendaAtualizada.Status = StatusVenda.AguardandoPagamento;

            _vendaRepository.Setup(s => s.Obter(It.IsAny<Guid>())).Returns(vendaPadrao);

            var vendaBusiness = new Venda(_vendaRepository.Object);

            Assert.ThrowsException<AlteracaoStatusException>(() => vendaBusiness.AtualizarVendaExecutor(vendaAtualizada));
        }

        [TestMethod]
        public void VendaBusiness_AtualizarVenda_Enviado_PagamentoAprovado_NOK()
        {
            var vendaPadrao = ObterVendaPadrao();
            var vendaAtualizada = ObterVendaPadrao();

            vendaPadrao.Status = StatusVenda.EnviadoParaTransportadora;
            vendaAtualizada.Status = StatusVenda.PagamentoAprovado;

            _vendaRepository.Setup(s => s.Obter(It.IsAny<Guid>())).Returns(vendaPadrao);

            var vendaBusiness = new Venda(_vendaRepository.Object);

            Assert.ThrowsException<AlteracaoStatusException>(() => vendaBusiness.AtualizarVendaExecutor(vendaAtualizada));
        }

        [TestMethod]
        public void VendaBusiness_AtualizarVenda_Enviado_Cancelada_NOK()
        {
            var vendaPadrao = ObterVendaPadrao();
            var vendaAtualizada = ObterVendaPadrao();

            vendaPadrao.Status = StatusVenda.EnviadoParaTransportadora;
            vendaAtualizada.Status = StatusVenda.Cancelada;

            _vendaRepository.Setup(s => s.Obter(It.IsAny<Guid>())).Returns(vendaPadrao);

            var vendaBusiness = new Venda(_vendaRepository.Object);

            Assert.ThrowsException<AlteracaoStatusException>(() => vendaBusiness.AtualizarVendaExecutor(vendaAtualizada));
        }

        #region Métodos Auxiliares
        private VendaModel ObterVendaPadrao()
        {
            var _vendaBuider = new VendaBuilder();
            var _produtoBuilder = new ProdutoBuilder();
            var _vendedorBuilder = new VendedorBuilder();


            #region Builder
            _produtoBuilder.ComId(Guid.NewGuid());
            _produtoBuilder.ComVendaId(Guid.NewGuid());
            _produtoBuilder.ComPreco(10);
            _produtoBuilder.ComNome("Nome Teste");

            var produto = _produtoBuilder.Build();

            _vendedorBuilder.ComId(Guid.NewGuid());
            _vendedorBuilder.ComVendaId(Guid.NewGuid());
            _vendedorBuilder.ComTelefone("31999999999");
            _vendedorBuilder.ComNome("Nome Teste");
            _vendedorBuilder.ComCPF("754.966.340-83");
            _vendedorBuilder.ComEmail("email@teste.com");

            var vendedor = _vendedorBuilder.Build();

            _vendaBuider.ComId(Guid.NewGuid());
            _vendaBuider.ComData(DateTime.Now);
            _vendaBuider.ComStatus(StatusVenda.AguardandoPagamento);
            _vendaBuider.ComVendedor(vendedor);
            _vendaBuider.ComProdutos(new List<ProdutoModel>() { produto });
            #endregion

            return _vendaBuider.Build();
        }
        #endregion


    }
}